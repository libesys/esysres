# ESys Resources

Note that this document is automatically generated from the file readme.xml by the tool
${msword2md_version}. So don’t modify the README.md file directly because all changes will be
lost. Modify the readme.xml instead.

This git repository holds various icons, pictures, etc. used in ESys software.

# Windows and icons

For Windows to display nicely the icon of an application, a multi-images icon must be added to the
resource file. To create such icon, the easiest is to use [ImageMagick](https://imagemagick.org)
convert command line tool.

To create the icon used by all ESys application, the following command was used:

> $ convert libesys_no_text_128px.png libesys_no_text_16px.png libesys_no_text_24px.png
> libesys_no_text_256px.png libesys_no_text_32px.png libesys_no_text_48px.png
> libesys_no_text_128px.png libesys_no_text_64px.png -adjoin libesys_no_text.ico

# Tools

The main tools used to handle the icons and images are:

* Online converter: [https://convertio.co](https://convertio.co)

* ImageMagick: [https://imagemagick.org](https://imagemagick.org)

* InkScape: [https://inkscape.org](https://inkscape.org)

> 

